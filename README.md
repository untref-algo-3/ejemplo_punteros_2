# Ejemplo de implementación práctica de la segunda clase teórica de Punteros

En este ejemplo se detalla el código para realizar los pasos descriptos en la segunda clase teórica de punteros para implementar funcionalidades adicionales a una lista simplemente enlazada. Recuerden mirar las diapositivas de la clase teórica y el video de la clase práctica primero donde realizamos esta implementación paso a paso.

Las operaciones adicionales que agregamos a la lista implementada en la primera clase son:

* Imprimir: recorre una lista e imprime el contenido de cada nodo.
* Eliminar: elimina un nodo de la lista, puede ser según la posición o por el dato.

Adicionalmente se modifica la implementación para modularizar correctamente las operaciones, migrando a un esquema de Tipo de Dato Abstracto (TDA).

Para ejecutar en la máquina virtual:

```
gcc lista_enlazada.c main.c -o lista_enlazada
./lista_enlazada
```
